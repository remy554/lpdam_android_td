package com.example.remy5.myapplication

import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import org.jetbrains.anko.toast

class MainActivityPreferences : AppCompatActivity() {

    companion object {

        val name = "userName"
        val vibrations = "vibrations"
        val vitesse = "vitesse"
        val rate = "rating"
    }

    private var prefName: String by DelegatesExt.preference(this, name, "Invité")
    private var prefVib: Boolean by DelegatesExt.preference(this, vibrations, false)
    private var prefVit: Int by DelegatesExt.preference(this, vitesse, -1)
    private var prefRate: Int by DelegatesExt.preference(this, rate, 0)


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preferences)

        var valider = findViewById(R.id.bt_valider) as Button
        var tv_name = findViewById(R.id.TV_name) as TextView
        var vibrations = findViewById(R.id.SW_vibrations) as Switch
        var vitesse = findViewById(R.id.SB_vitesse) as SeekBar
        var bar = findViewById(R.id.ratingBar) as RatingBar

        tv_name.text = prefName
        vibrations.isChecked = prefVib;
        vitesse.progress = prefVit
        bar.rating = prefRate.toFloat()

        valider.setOnClickListener {

            prefName = tv_name.text.toString()
            prefVib = vibrations.isChecked
            prefVit = vitesse.progress
            prefRate = bar.rating.toInt()

            toast("Préférences sauvegardées ")

            val intent = Intent(this, MainActivity::class.java)
           // intent.putExtra("nomUser", prefName)
            startActivity(intent)

        }


    }
}
