package com.example.remy5.myapplication;

        import android.app.AlertDialog;
        import android.app.Dialog;
        import android.graphics.drawable.Drawable;
        import android.support.v7.widget.RecyclerView;
        import android.util.Pair;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageButton;
        import android.widget.ImageView;
        import android.widget.TextView;

        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private List<Pair<String, String>> characters = Arrays.asList(
                Pair.create("Honeycomb", "Version 3.0")

            );

    private List<Drawable> images = new ArrayList<Drawable>();

    public MyAdapter(List<Pair<String, String>> characters, ArrayList<Drawable> images){
        this.characters = characters;
        this.images = images;
    }



    @Override
    public int getItemCount() {
        return characters.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_recycler, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pair<String, String> pair = characters.get(position);
        Drawable image = images.get(position);
        holder.display(pair, image);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final ImageView logo;
        private final TextView name;
        private final TextView description;

        private Pair<String, String> currentPair;

        public MyViewHolder(final View itemView) {
            super(itemView);

            name = ((TextView) itemView.findViewById(R.id.name));
            description = ((TextView) itemView.findViewById(R.id.desc));
            logo = ((ImageView) itemView.findViewById(R.id.imageView));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(currentPair.first)
                            .setMessage(currentPair.second)
                            .show();
                }
            });
        }

        public void display(Pair<String, String> pair, Drawable image) {
            currentPair = pair;
            name.setText(pair.first);
            description.setText(pair.second);
            logo.setImageDrawable(image);
        }
    }

}