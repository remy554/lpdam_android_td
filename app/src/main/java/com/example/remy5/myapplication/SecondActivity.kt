package com.example.remy5.myapplication

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Vibrator
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Pair
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import org.jetbrains.anko.toast
import java.util.*
import android.content.Context.VIBRATOR_SERVICE
import org.w3c.dom.Text


class SecondActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var prefName: String by DelegatesExt.preference(this, MainActivityPreferences.name, "Invité")
    private var prefVib: Boolean by DelegatesExt.preference(this, MainActivityPreferences.vibrations, false)
    private var prefVit: Int by DelegatesExt.preference(this, MainActivityPreferences.vitesse, -1)
    private var prefRate: Int by DelegatesExt.preference(this, MainActivityPreferences.rate, 0)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val fab = findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()



        val navigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        var navHeaderView = navigationView.getHeaderView(0)

        var nom = navHeaderView.findViewById<TextView>(R.id.TV_nom_menu)
        nom.text = prefName //affiche le nom choisi dans "Mes préférences" dans le menu de gauche

        val r = findViewById(R.id.recyclerView) as RecyclerView
        r.visibility = View.GONE

        val dureeVib = prefVit.toLong()+1000
        var vibrations = findViewById(R.id.TV_vibrations) as TextView
        var vitesse = findViewById(R.id.TV_vitesse) as TextView
        var note = findViewById(R.id.TV_rating) as TextView
        var nomuser = findViewById(R.id.TV_nom_user) as TextView

        if(prefVib){
            val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(dureeVib)
            vibrations.text = "Les vibrations sont activées comme vous pouvez le sentir !"
            vitesse.visibility = View.VISIBLE
            vitesse.text = "La durée des vibrations est définie par la SeekBar depuis les réglages"

        }else{
            vibrations.text = "Les vibrations sont désactivées, changez cela depuis les réglages"
            vitesse.visibility = View.GONE

        }

        if(prefRate != 0){
            note.text = "Merci d'avoir noté l'application " + prefRate + " étoiles !"
        }else{
            note.text = "Pensez à noter l'application !"
        }

        nomuser.text = "Le nom que vous avez indiqué est affiché dans le menu de gauche, " + prefName + "!"










    }


    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            //Toast.makeText(this, "Paramètres", Toast.LENGTH_LONG).show()
           /* alert("Ouverture d'une popup grace à la fonction alert de la librairie Anko") {
                yesButton { toast("Vous avez validé votre choix ") }
                noButton {}
            }.show()*/

            val intent = Intent(this, MainActivityPreferences::class.java)
            startActivity(intent)

            true
        } else super.onOptionsItemSelected(item)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        val ft = fragmentManager.beginTransaction()



        if (id == R.id.nav_camera) {

            ft.replace(R.id.liste_frag, MonFragment())
            ft.commit()

        } else if (id == R.id.nav_share) {
            toast("Vous êtes déjà sur cet onglet ")
        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

}